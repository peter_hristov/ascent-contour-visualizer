//============================================================================
// Copyright (c) 2019, The Regents of the University of California, through
// Lawrence Berkeley National Laboratory (subject to receipt of any required approvals
// from the U.S. Dept. of Energy).  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// (1) Redistributions of source code must retain the above copyright notice, this
//     list of conditions and the following disclaimer.
//
// (2) Redistributions in binary form must reproduce the above copyright notice,
//     this list of conditions and the following disclaimer in the documentation
//     and/or other materials provided with the distribution.
//
// (3) Neither the name of the University of California, Lawrence Berkeley National
//     Laboratory, U.S. Dept. of Energy nor the names of its contributors may be
//     used to endorse or promote products derived from this software without
//     specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
// OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//============================================================================

#include "./mc.h"

#include <vtkm/filter/Contour.h>

using namespace std;
using namespace vtkm;

cont::ArrayHandle<cv1k::Triangle> cv1k::mc::getMarchingCubeTriangles(cont::DataSet inputData, vector<Float64> isovalues, std::string fieldName)
{
    // Use Marching Cubes Filter
    filter::Contour filter;
    filter.SetGenerateNormals(true);
    filter.SetAddInterpolationEdgeIds(true);
    filter.SetMergeDuplicatePoints(true);
    filter.SetIsoValues({isovalues});
    filter.SetActiveField(fieldName);
    cont::DataSet outputData = filter.Execute(inputData);

    const auto coordinatesPortal = outputData.GetCoordinateSystem().GetData().Cast<cont::ArrayHandle<vtkm::Vec<float, 3>>>().ReadPortal();

    // Cast cells to a concrete data structure we can work with
    cont::CellSetSingleType<> cellSet;
    outputData.GetCellSet().CopyTo(cellSet);

    // Set up array to hold triangles
    cont::ArrayHandle<cv1k::Triangle> triangles;
    triangles.Allocate(cellSet.GetNumberOfCells());

    // Portal that reads the enpoints of the edges intersected by the MC triangles
    auto edgeIdPortal = outputData.GetField("edgeIds").GetData().Cast<vtkm::cont::ArrayHandle<Vec<vtkm::Id, 2>>>().GetPortalConstControl();

    // Extract triangles from the MC output cell mesh
    for (Id i = 0; i < cellSet.GetNumberOfCells(); i++)
    {
        // Read the indices of the points of a triangle(in the marching cubes outputData)
        Id pointIds[3];
        cellSet.GetCellPointIds(i, pointIds);

        // Set the triangle id and coordinates
        triangles.GetPortalControl().Set(i,
            { -1,
                edgeIdPortal.Get(pointIds[0]),
                { coordinatesPortal.Get(pointIds[0]), coordinatesPortal.Get(pointIds[1]), coordinatesPortal.Get(pointIds[2]) } });
    }

    return triangles;
}

